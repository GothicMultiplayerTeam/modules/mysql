include(Fetch_Gitlab_Release)

string(TOLOWER "${CMAKE_SYSTEM_NAME}" TARGET_SYSTEM_NAME)
string(TOLOWER "${CMAKE_BUILD_TYPE}" TARGET_BUILD_TYPE)

find_package(unofficial-libmariadb CONFIG REQUIRED)

add_subdirectory(sqapi)
add_subdirectory(readerwriterqueue)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		sqapi
		readerwriterqueue
		unofficial::libmariadb
)