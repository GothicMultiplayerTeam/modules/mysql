function (Fetch_Gitlab_Release)
	cmake_parse_arguments(Download_Gitlab_Release "" "URL;LINK_NAME;DESTINATION;EXISTING_PATH" "" ${ARGN})

	if (NOT DEFINED Download_Gitlab_Release_URL)
		message(SEND_ERROR "Download_Gitlab_Release function requires URL argument")
		return()
	endif()

	if (NOT DEFINED Download_Gitlab_Release_LINK_NAME)
		message(SEND_ERROR "Download_Gitlab_Release: function requires LINK_NAME argument")
		return()
	endif()

	if (NOT DEFINED Download_Gitlab_Release_DESTINATION)
		message(SEND_ERROR "Download_Gitlab_Release: function requires DESTINATION argument")
		return()
	endif()

	if (NOT DEFINED Download_Gitlab_Release_EXISTING_PATH)
		message(SEND_ERROR "Download_Gitlab_Release: function requires EXISTING_PATH argument")
		return()
	endif()

	if (EXISTS ${Download_Gitlab_Release_EXISTING_PATH})
		# file downloaded and installed, nothing to do
		return()
	endif()

	# Extract project name, path from url
	# example url: https://gitlab.com/GothicMultiplayerTeam/dependencies/openssl/-/releases/1.1.1t
	# will extract this path: GothicMultiplayerTeam%2Fdependencies%2Fopenssl
	# will extract this name: openssl
	string(REGEX MATCH "https://(www\\.)?gitlab.com/(.*)/-/releases/" project_path ${Download_Gitlab_Release_URL})
	set(project_path ${CMAKE_MATCH_2})
	string(REGEX MATCH "([^/]+)$" project_name "${project_path}")
	string(REPLACE "/" "%2F" project_path ${project_path})
	

	# Extract release tag from url
	# example url: https://gitlab.com/GothicMultiplayerTeam/dependencies/openssl/-/releases/1.1.1t
	# will extract this part: 1.1.1t
	string(REGEX MATCH "releases/([^/]+)$" release_tag ${Download_Gitlab_Release_URL})
	set(release_tag ${CMAKE_MATCH_1})

	file(DOWNLOAD "https://gitlab.com/api/v4/projects/${project_path}"
			"${CMAKE_CURRENT_BINARY_DIR}/gitlab_cache/${project_name}_info.json"
	)

	# Send gitlab API request to extract project id
	file(READ "${CMAKE_CURRENT_BINARY_DIR}/gitlab_cache/${project_name}_info.json" project_json_info)
	string(REGEX MATCH "\"id\":([0-9]+)" project_id "${project_json_info}")
	
	set(project_id ${CMAKE_MATCH_1})
	file(REMOVE "${CMAKE_CURRENT_BINARY_DIR}/gitlab_cache/${project_name}_info.json")

	# Send gitlab API request to extract releases page links
	file(DOWNLOAD "https://gitlab.com/api/v4/projects/${project_id}/releases/${release_tag}/assets/links"
		"${CMAKE_CURRENT_BINARY_DIR}/gitlab_cache/${project_name}_releases.json"
	)
	file(READ "${CMAKE_CURRENT_BINARY_DIR}/gitlab_cache/${project_name}_releases.json" release_json_urls)
	file(REMOVE "${CMAKE_CURRENT_BINARY_DIR}/gitlab_cache/${project_name}_releases.json")
	
	string(JSON release_json_urls_len LENGTH ${release_json_urls})
	math(EXPR release_json_urls_len "${release_json_urls_len} - 1")

	set(release_url "")

	foreach(idx RANGE ${release_json_urls_len})
	    string(JSON name GET ${release_json_urls} ${idx} "name")
	    string(JSON url GET ${release_json_urls} ${idx} "url")
	
		if (${name} STREQUAL ${Download_Gitlab_Release_LINK_NAME})
			set(release_url ${url})
			break()
		endif()
	endforeach()

	if (release_url STREQUAL "")
		message(SEND_ERROR "Download_Gitlab_Release: LINK_NAME:\"${Download_Gitlab_Release_LINK_NAME}\" not found in gilab release page")
		return()
	endif()

	# Replace '/' with '-' to be able to use the link name as filename
	string(REPLACE "/" "-" Download_Gitlab_Release_LINK_NAME ${Download_Gitlab_Release_LINK_NAME})

	file(DOWNLOAD ${release_url}
		"${CMAKE_CURRENT_BINARY_DIR}/gitlab_cache/${project_name}_${Download_Gitlab_Release_LINK_NAME}.zip"
	    SHOW_PROGRESS
	)
	
	file(ARCHIVE_EXTRACT
		INPUT "${CMAKE_CURRENT_BINARY_DIR}/gitlab_cache/${project_name}_${Download_Gitlab_Release_LINK_NAME}.zip"
		DESTINATION "${Download_Gitlab_Release_DESTINATION}"
	)

	file(REMOVE "${CMAKE_CURRENT_BINARY_DIR}/gitlab_cache/${project_name}_${Download_Gitlab_Release_LINK_NAME}.zip")
endfunction()