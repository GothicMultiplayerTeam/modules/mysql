# MySQL

## Introduction

MySQL module binds a third-party mysql library and provides squirrel functions to access data from MySQL databases.  
More information about MySQL database you will find on the internet.

If you looking for SQL language tutorial look here: https://www.w3schools.com/sql/  
Used SDK: https://downloads.mysql.com/archives/c-c/

# Build requirements

On top of [CMake](https://cmake.org/download/), you also need to install [vcpkg](https://learn.microsoft.com/en-us/vcpkg/get_started/get-started?pivots=shell-powershell) to be able to build the project.

## Documentation

https://gothicmultiplayerteam.gitlab.io/modules/mysql/