This example demonstrates overall usage of async API via native functions provided by the module.

```js
mysql_connect_async("127.0.0.1", "root", "", "test_db", null, null, function(conn)
{
	if (!conn)
		return print("Connection with db can't be establied!\nError: " + mysql_error() + "\n Code: " + mysql_errno())

	print("connection with db established")
	print(conn)

	mysql_set_character_set_async(conn, "cp1250", function(result)
	{
		print("charset: " + result)
	})

	mysql_real_escape_string_async(conn, "SELECT * FROM \"quoted text\"", function(result)
	{
		print("escaped text: " + result)
	})

	mysql_query_async(conn, "SELECT * FROM users", function(result)
	{
		print("got result: " + result)

		local row = null
		while (row = mysql_fetch_assoc(result))
		{
			print("id: " + row.id + " name = \"" + row.name + "\"")
		}
	})

	mysql_close_async(conn, function()
	{
		print("connection closed!")
	})
})
```

This example demonstrates overall usage of async API via [**async**](https://gitlab.com/g2o/scripts/async) script package.
```js
async(function(thread)
{
    local promise = await(thread, mysql_connect_async, "127.0.0.1", "root", "", "db", null, null)
    local conn = promise.result

    if (!conn)
        throw mysql_error_async()

    promise = await(thread, mysql_query_async, conn, "SELECT * FROM users")
    local result = promise.result

    if (!result)
        throw mysql_error_async(conn)

    local row = null
    while (row = mysql_fetch_assoc(result))
    {
        print("id: " + row.id + " name = \"" + row.name + "\"")
    }

    await(thread, mysql_close_async, conn)
})
```