#include "g2o_api.h"

Sqrat::Function addEventHandler(const char* eventName, SQFUNCTION closure, SQInteger priority)
{
	using namespace SqModule;

	HSQOBJECT closureHandle;

	sq_newclosure(vm, closure, 0);
	sq_getstackobj(vm, -1, &closureHandle);

	Sqrat::Function func(vm, Sqrat::RootTable().GetObject(), closureHandle);
	Sqrat::RootTable().GetFunction("addEventHandler")(eventName, func, priority);

	sq_pop(vm, 1);

	return func;
}

SQInteger setTimer(SQFUNCTION closure, SQInteger interval, SQInteger count)
{
	using namespace SqModule;

	HSQOBJECT closureHandle;

	sq_newclosure(vm, closure, 0);
	sq_getstackobj(vm, -1, &closureHandle);

	Sqrat::Function func(vm, Sqrat::RootTable().GetObject(), closureHandle);
	auto result = Sqrat::RootTable().GetFunction("setTimer").Evaluate<SQInteger>(func, interval, count);

	sq_pop(vm, 1);

	return *result;
}

SQInteger getTimerInterval(SQInteger timer_id)
{
	auto result = Sqrat::RootTable().GetFunction("getTimerInterval").Evaluate<SQInteger>(timer_id);
	return *result;
}

void setTimerInterval(SQInteger timer_id, SQInteger interval)
{
	Sqrat::RootTable().GetFunction("setTimerInterval")(timer_id, interval);
}