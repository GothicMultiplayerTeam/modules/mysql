#pragma once

#include <memory>
#include <mutex>
#include <readerwriterqueue.h>
#include <mysql.h>

#include "mysql_async_result.h"

struct mysql_async_connection
{
	size_t thread_id = -1;
	MYSQL* handler = nullptr;
	moodycamel::ReaderWriterQueue<std::unique_ptr<mysql_async_result>> results;
	std::mutex mutex;
};