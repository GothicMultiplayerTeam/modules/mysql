#pragma once

#include <vector>

#include "mysql_async_thread.h"

class mysql_async_threadpool
{
public:
	mysql_async_threadpool(size_t thread_count);

	mysql_async_thread& get_async_thread(size_t id);
	size_t get_next_async_thread_id();
	size_t get_thread_count();
	void resize(size_t size);
	void reset();

private:
	std::vector<std::unique_ptr<mysql_async_thread>> threads_;
};