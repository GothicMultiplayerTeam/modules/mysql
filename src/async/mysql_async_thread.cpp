#include "mysql_async_thread.h"
#include "mysql_async_connection.h"

#include <mysql.h>
#include <utility>

extern bool mysql_opt_reconnect;

mysql_async_thread::mysql_async_thread() :
	thread_(&mysql_async_thread::worker, this)
{
}

mysql_async_thread::~mysql_async_thread()
{
	stop_thread();
}

void mysql_async_thread::add_task(mysql_async_task task)
{
	tasks_.enqueue(std::move(task));
	cv_.notify_one();
}

void mysql_async_thread::stop_thread()
{
	thread_running_ = false;

	if (thread_.joinable())
	{
		cv_.notify_one();
		thread_.join();
	}
}

void mysql_async_thread::worker()
{
	while (thread_running_)
	{
		std::unique_lock<std::mutex> lock(this->mutex_);
		this->cv_.wait(lock, [this]() { return this->tasks_.size_approx() > 0 || !thread_running_; });
		lock.unlock();

		mysql_async_task task;
		while (this->tasks_.try_dequeue(task))
		{
			task.conn->mutex.lock();
			task.thread_callback(task);
			task.conn->mutex.unlock();

			if (task.result.get())
				task.conn->results.enqueue(std::move(task.result));
		}
	}
}