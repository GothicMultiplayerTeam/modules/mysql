#pragma once

#include <thread>
#include <mutex>
#include <condition_variable>
#include <readerwriterqueue.h>

#include "mysql_async_task.h"
#include "mysql_async_result.h"

class mysql_async_thread
{
public:
	mysql_async_thread();
	~mysql_async_thread();

	void add_task(mysql_async_task task);

private:
	void stop_thread();
	void worker();

	std::thread thread_;
	volatile bool thread_running_ = true;

	std::mutex mutex_;
	std::condition_variable cv_;

	moodycamel::ReaderWriterQueue<mysql_async_task> tasks_;
};