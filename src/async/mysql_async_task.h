#pragma once

#include "mysql_async_result.h"

#include <sqapi.h>
#include <vector>
#include <memory>
#include <functional>

class mysql_async_connection;

struct mysql_async_task
{
	mysql_async_connection* conn;
	std::function<void(mysql_async_task& task)> thread_callback;
	std::unique_ptr<mysql_async_result> result;
};