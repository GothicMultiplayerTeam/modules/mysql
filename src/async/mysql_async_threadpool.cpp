#include "mysql_async_threadpool.h"
#include "mysql_async_threadpool.h"

mysql_async_threadpool::mysql_async_threadpool(size_t thread_count)
{
	resize(thread_count);
}

mysql_async_thread& mysql_async_threadpool::get_async_thread(size_t id)
{
	return *threads_[id].get();
}

size_t mysql_async_threadpool::get_next_async_thread_id()
{
	static size_t current_thread_id = 0;
	if (++current_thread_id >= threads_.size())
		current_thread_id = 0;

	return current_thread_id;
}

size_t mysql_async_threadpool::get_thread_count()
{
	return threads_.size();
}

void mysql_async_threadpool::resize(size_t size)
{
	if (size == 0)
		return;

	size_t old_size = threads_.size();
	threads_.resize(size);

	for (size_t i = old_size; i < size; ++i)
		threads_[i] = std::make_unique<mysql_async_thread>();
}

void mysql_async_threadpool::reset()
{
	threads_.clear();
}
