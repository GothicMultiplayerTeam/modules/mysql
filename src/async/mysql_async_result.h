#pragma once

#include <sqapi.h>
#include <vector>
#include <memory>
#include <functional>

class mysql_async_connection;

struct mysql_async_result
{
	mysql_async_connection* conn;
	std::function<bool(mysql_async_result& result)> mainthread_callback;
	SQUserPointer value = nullptr;

	mysql_async_result(mysql_async_connection* conn, std::function<bool(mysql_async_result& result)> mainthread_callback) :
		conn(conn),
		mainthread_callback(mainthread_callback)
	{
	}
};