#pragma once

#include <string>
#include <squirrel.h>

#include <variant>

struct mysql_connect_async_args
{
	std::string host;
	std::string user;
	std::string password;
	std::string database;
	SQInteger port;
	std::string socket;

	mysql_connect_async_args(const char* host, const char* user, const char* password, const char* database, SQInteger port, const char* socket) :
		host(host),
		user(user),
		password(password),
		database(database),
		port(port),
		socket(socket != nullptr ? socket : "")
	{}
};

struct mysql_select_db_async_args
{
	std::string database;

	mysql_select_db_async_args(const char* database) :
		database(database)
	{}
};

struct mysql_query_async_args
{
	std::string query;

	mysql_query_async_args(const char* query) :
		query(query)
	{}
};

struct mysql_set_character_set_async_args
{
	std::string charset;

	mysql_set_character_set_async_args(const char* charset) :
		charset(charset)
	{}
};

struct mysql_real_escape_string_async_args
{
	std::string old_text;
	size_t buffer_size;

	mysql_real_escape_string_async_args(const char* old_text, size_t buffer_size) :
		old_text(old_text),
		buffer_size(buffer_size)
	{}
};

using mysql_async_args = std::variant<std::monostate, mysql_connect_async_args, mysql_select_db_async_args, mysql_query_async_args, mysql_set_character_set_async_args, mysql_real_escape_string_async_args>;