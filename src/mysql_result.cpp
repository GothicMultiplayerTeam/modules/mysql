#include "mysql_result.h"

#include <mysql.h>

mysql_result::mysql_result(MYSQL_RES* res) :
	res(res)
{
}

mysql_result::~mysql_result()
{
	free();
}

void mysql_result::free()
{
	if (res != nullptr)
	{
		mysql_free_result(res);
		res = nullptr;
	}
}
