#pragma once

#include <sqapi.h>
#include <mysql.h>

class mysql_result
{
public:
	mysql_result(MYSQL_RES* result);
	~mysql_result();

	void free();

	MYSQL_RES* res = nullptr;
};