#pragma once

#include <sqapi.h>

SQInteger sq_onTimer(HSQUIRRELVM vm);
SQInteger sq_onExit(HSQUIRRELVM vm);

SQInteger sq_mysql_connect_async(HSQUIRRELVM vm);
SQInteger sq_mysql_close_async(HSQUIRRELVM vm);
SQInteger sq_mysql_select_db_async(HSQUIRRELVM vm);
SQInteger sq_mysql_query_async(HSQUIRRELVM vm);
SQInteger sq_mysql_multi_query_async(HSQUIRRELVM vm);
SQInteger sq_mysql_ping_async(HSQUIRRELVM vm);
SQInteger sq_mysql_set_character_set_async(HSQUIRRELVM vm);
SQInteger sq_mysql_real_escape_string_async(HSQUIRRELVM vm);
SQInteger sq_mysql_error_async(HSQUIRRELVM vm);
SQInteger sq_mysql_errno_async(HSQUIRRELVM vm);

SQInteger sq_mysql_connect(HSQUIRRELVM vm);
SQInteger sq_mysql_close(HSQUIRRELVM vm);
SQInteger sq_mysql_select_db(HSQUIRRELVM vm);
SQInteger sq_mysql_query(HSQUIRRELVM vm);
SQInteger sq_mysql_multi_query(HSQUIRRELVM vm);
SQInteger sq_mysql_insert_id(HSQUIRRELVM vm);
SQInteger sq_mysql_affected_rows(HSQUIRRELVM vm);
SQInteger sq_mysql_num_rows(HSQUIRRELVM vm);
SQInteger sq_mysql_num_fields(HSQUIRRELVM vm);
SQInteger sq_mysql_fetch_row(HSQUIRRELVM vm);
SQInteger sq_mysql_fetch_assoc(HSQUIRRELVM vm);
SQInteger sq_mysql_ping(HSQUIRRELVM vm);
SQInteger sq_mysql_free_result(HSQUIRRELVM vm);
SQInteger sq_mysql_error(HSQUIRRELVM vm);
SQInteger sq_mysql_errno(HSQUIRRELVM vm);
SQInteger sq_mysql_info(HSQUIRRELVM vm);
SQInteger sq_mysql_stat(HSQUIRRELVM vm);
SQInteger sq_mysql_sqlstate(HSQUIRRELVM vm);
SQInteger sq_mysql_warning_count(HSQUIRRELVM vm);
SQInteger sq_mysql_get_character_set_info(HSQUIRRELVM vm);
SQInteger sq_mysql_character_set_name(HSQUIRRELVM vm);
SQInteger sq_mysql_set_character_set(HSQUIRRELVM vm);
SQInteger sq_mysql_escape_string(HSQUIRRELVM vm);
SQInteger sq_mysql_real_escape_string(HSQUIRRELVM vm);
SQInteger sq_mysql_option_reconnect(HSQUIRRELVM vm);
SQInteger sq_mysql_option_thread_count(HSQUIRRELVM vm);
SQInteger sq_mysql_option_result_timer_interval(HSQUIRRELVM vm);