#include "sqfunc.h"

#include "mysql_result.h"
#include "g2o_api.h"

#include "async/mysql_async_args.h"
#include "async/mysql_async_connection.h"
#include "async/mysql_async_threadpool.h"

#include <cstring>
#include <memory>
#include <vector>
#include <unordered_set>
#include <unordered_map>

#include <mysql.h>
#include <sqapi.h>

#define ASYNC_MUTEX_GUARD(code) \
	auto it = async_connections_lookup.find(reinterpret_cast<MYSQL*>(handler)); \
	if (it != async_connections_lookup.end()) \
		it->second->mutex.lock(); \
	code; \
	if (it != async_connections_lookup.end()) \
		it->second->mutex.unlock();

// shared variables

bool mysql_opt_reconnect = false;

// sync variables

static int sync_cached_errno = 0;
static std::string sync_cached_error = "";

static std::unordered_set<MYSQL*> sync_connections_lookup;

// async variables

static std::mutex async_cached_error_mutex;
static int async_cached_errno = 0;
static std::string async_cached_error = "";

static std::vector<std::unique_ptr<mysql_async_connection>> async_connections;
static std::unordered_map<MYSQL*, mysql_async_connection*> async_connections_lookup;

static mysql_async_threadpool async_threadpool(1);

extern int mysql_async_timer_id;

// async response handler

SQInteger sq_onTimer(HSQUIRRELVM vm)
{
	// IMPORTANT! Do not even think about replacing this loop with c++ foreach
	// The reason why this won't work is the fact that scripters might open new connections
	// when another connection has been established (inside the callback)
	// Also, don't even think about using reference to connection, because it might get invalidated
	// due to reallocation of `async_connections`

	for (size_t i = 0, end = async_connections.size(); i < end; ++i)
	{
		std::unique_ptr<mysql_async_result> result;
		while (async_connections[i]->results.try_dequeue(result))
		{
			if (result->mainthread_callback(*result.get()))
			{
				async_connections[i].reset();
				break;
			}
		}
	}

	async_connections.erase(std::remove_if(async_connections.begin(), async_connections.end(), [](auto& conn) { return conn == nullptr;}), async_connections.end());
	return 0;
}

SQInteger sq_onExit(HSQUIRRELVM vm)
{
	async_threadpool.reset();

	for (auto& conn : async_connections)
		conn.reset();

	return 0;
}

// helpers

static bool is_valid_mysql_sync_connection(MYSQL* handler)
{
	return sync_connections_lookup.find(handler) != sync_connections_lookup.end();
}

static bool is_valid_mysql_async_connection(MYSQL* handler)
{
	return async_connections_lookup.find(handler) != async_connections_lookup.end();
}

static bool is_valid_mysql_connection(MYSQL* handler)
{
	return handler && (is_valid_mysql_sync_connection(handler) || is_valid_mysql_async_connection(handler));
}

// async functions

SQInteger sq_mysql_connect_async(HSQUIRRELVM vm)
{
	HSQOBJECT env;
	sq_getstackobj(vm, 1, &env);

	const SQChar* host;
	sq_getstring(vm, 2, &host);

	const SQChar* user;
	sq_getstring(vm, 3, &user);

	const SQChar* password;
	sq_getstring(vm, 4, &password);

	const SQChar* database;
	sq_getstring(vm, 5, &database);

	SQInteger port = 3306;
	if (sq_gettype(vm, 6) == OT_INTEGER)
		sq_getinteger(vm, 6, &port);

	const SQChar* socket = nullptr;
	if (sq_gettype(vm, 7) == OT_STRING)
		sq_getstring(vm, 7, &socket);

	HSQOBJECT closure;
	sq_getstackobj(vm, 8, &closure);

	std::unique_ptr<mysql_async_connection> connection = std::make_unique<mysql_async_connection>();
	connection->thread_id = async_threadpool.get_next_async_thread_id();

	mysql_async_connection* conn = connection.get();
	auto args = mysql_connect_async_args(host, user, password, database, port, socket);

	mysql_async_task task {
		conn,

		[args](mysql_async_task& task)
		{
			MYSQL* handler = mysql_init(NULL);

			if (handler)
				mysql_options(handler, MYSQL_OPT_RECONNECT, &mysql_opt_reconnect);

			if (!mysql_real_connect(handler, args.host.c_str(), args.user.c_str(), args.password.c_str(), args.database.c_str(), args.port, args.socket.length() ? args.socket.c_str() : nullptr, CLIENT_MULTI_STATEMENTS))
			{
				async_cached_error_mutex.lock();
				async_cached_errno = mysql_errno(handler);
				async_cached_error = mysql_error(handler);
				async_cached_error_mutex.unlock();

				mysql_close(handler);
				handler = nullptr;
			}

			task.conn->handler = handler;
			task.result->value = handler;
		},

		std::make_unique<mysql_async_result>(
			conn,

			[callback = Sqrat::Function(SqModule::vm, env, closure)](mysql_async_result& result) mutable
			{
				if (result.value == nullptr)
				{
					callback(nullptr);
					return true;
				}

				async_connections_lookup[reinterpret_cast<MYSQL*>(result.value)] = result.conn;
				callback(result.value);

				if (Sqrat::Error::Occurred(SqModule::vm))
					Sqrat::Error::Clear(SqModule::vm);

				return false;
			}
		)
	};

	async_threadpool.get_async_thread(connection->thread_id).add_task(std::move(task));
	async_connections.emplace_back(std::move(connection));

	return 0;
}

SQInteger sq_mysql_close_async(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 3)
		return sq_throwerror(vm, "wrong number of parameters");

	HSQOBJECT env;
	sq_getstackobj(vm, 1, &env);

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	HSQOBJECT closure;
	sq_getstackobj(vm, 3, &closure);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_async_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	mysql_async_connection* conn = async_connections_lookup[handler];

	mysql_async_task task{
		conn,

		[](mysql_async_task& task)
		{
			mysql_close(task.conn->handler);
		},

		closure._type == OT_NULL ? nullptr : std::make_unique<mysql_async_result>(
			conn,

			[callback = Sqrat::Function(SqModule::vm, env, closure)] (mysql_async_result& result) mutable
			{
				callback();

				if (Sqrat::Error::Occurred(SqModule::vm))
					Sqrat::Error::Clear(SqModule::vm);

				return true;
			}
		)
	};

	async_threadpool.get_async_thread(conn->thread_id).add_task(std::move(task));
	async_connections_lookup.erase(conn->handler);
	
	return 0;
}

SQInteger sq_mysql_select_db_async(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 4)
		return sq_throwerror(vm, "wrong number of parameters");

	HSQOBJECT env;
	sq_getstackobj(vm, 1, &env);

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	const SQChar* database;
	sq_getstring(vm, 3, &database);

	HSQOBJECT closure;
	sq_getstackobj(vm, 4, &closure);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_async_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	mysql_async_connection* conn = async_connections_lookup[handler];
	auto args = mysql_select_db_async_args(database);

	mysql_async_task task {
		conn,

		[args](mysql_async_task& task)
		{
			SQUserPointer value = reinterpret_cast<SQUserPointer>(mysql_select_db(task.conn->handler, args.database.c_str()) == 0);
			if (task.result)
				task.result->value = value;
		},

		closure._type == OT_NULL ? nullptr : std::make_unique<mysql_async_result>(
			conn,

			[callback = Sqrat::Function(SqModule::vm, env, closure)](mysql_async_result& result) mutable
			{
				callback(result.value == reinterpret_cast<SQUserPointer>(1));

				if (Sqrat::Error::Occurred(SqModule::vm))
					Sqrat::Error::Clear(SqModule::vm);

				return false;
			}
		)
	};

	async_threadpool.get_async_thread(conn->thread_id).add_task(std::move(task));
	
	return 0;
}

SQInteger sq_mysql_query_async(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 4)
		return sq_throwerror(vm, "wrong number of parameters");

	HSQOBJECT env;
	sq_getstackobj(vm, 1, &env);

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	const SQChar* query;
	sq_getstring(vm, 3, &query);

	HSQOBJECT closure;
	sq_getstackobj(vm, 4, &closure);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_async_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	mysql_async_connection* conn = async_connections_lookup[handler];
	auto args = mysql_query_async_args(query);

	mysql_async_task task {
		conn,

		[args](mysql_async_task& task)
		{
			mysql_set_server_option(task.conn->handler, MYSQL_OPTION_MULTI_STATEMENTS_OFF);
			SQUserPointer value = !mysql_query(task.conn->handler, args.query.c_str()) ? reinterpret_cast<SQUserPointer>(mysql_store_result(task.conn->handler)) : nullptr;

			if (task.result)
			{
				task.result->conn = std::move(task.conn);
				task.result->value = value;
			}
		},

		closure._type == OT_NULL ? nullptr : std::make_unique<mysql_async_result>(
			conn,
			
			[callback = Sqrat::Function(SqModule::vm, env, closure)](mysql_async_result& result) mutable
			{
				if (result.value == nullptr)
					callback(nullptr);
				else
				{
					Sqrat::Object obj_result(new mysql_result(reinterpret_cast<MYSQL_RES*>(result.value)), SqModule::vm, true);
					callback(obj_result);
				}

				if (Sqrat::Error::Occurred(SqModule::vm))
					Sqrat::Error::Clear(SqModule::vm);
				
				return false;
			}
		)
	};

	async_threadpool.get_async_thread(conn->thread_id).add_task(std::move(task));

	return 0;
}

SQInteger sq_mysql_multi_query_async(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 4)
		return sq_throwerror(vm, "wrong number of parameters");

	HSQOBJECT env;
	sq_getstackobj(vm, 1, &env);

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	const SQChar* query;
	sq_getstring(vm, 3, &query);

	HSQOBJECT closure;
	sq_getstackobj(vm, 4, &closure);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_async_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	mysql_async_connection* conn = async_connections_lookup[handler];
	auto args = mysql_query_async_args(query);

	mysql_async_task task {
		conn,

		[args](mysql_async_task& task)
		{
			mysql_set_server_option(task.conn->handler, MYSQL_OPTION_MULTI_STATEMENTS_ON);

			int status = mysql_query(task.result->conn->handler, args.query.c_str());
			if (status)
			{
				task.result->value = nullptr;
				return;
			}

			auto* results = static_cast<std::vector<MYSQL_RES*>*>(new std::vector<MYSQL_RES*>());
			task.result->value = results;

			do
			{
				MYSQL_RES* res = mysql_store_result(task.conn->handler);
				if (res)
					results->push_back(res);
				else if (mysql_errno(task.conn->handler) != 0)
				{
					delete results;
					task.result->value = nullptr;

					return;
				}

				if ((status = mysql_next_result(task.conn->handler)) > 0)
				{
					delete results;
					task.result->value = nullptr;

					return;
				}
			} while (status == 0);
		},

		std::make_unique<mysql_async_result>(
			conn,

			[callback = closure._type == OT_NULL ? Sqrat::Function() : Sqrat::Function(SqModule::vm, env, closure)](mysql_async_result& result) mutable
			{
				auto* results = static_cast<std::vector<MYSQL_RES*>*>(result.value);

				if (callback.IsNull())
				{
					if (results)
						delete results;

					return false;
				}

				if (results == nullptr)
					callback(nullptr);
				else
				{
					Sqrat::Array array(SqModule::vm);

					for (MYSQL_RES* res : *results)
						array.Append(Sqrat::Object(new mysql_result(res), SqModule::vm, true));

					delete results;
					callback(array);
				}

				if (Sqrat::Error::Occurred(SqModule::vm))
					Sqrat::Error::Clear(SqModule::vm);

				return false;
			}
		)
	};

	async_threadpool.get_async_thread(conn->thread_id).add_task(std::move(task));

	return 0;
}

SQInteger sq_mysql_ping_async(HSQUIRRELVM vm)
{
	HSQOBJECT env;
	sq_getstackobj(vm, 1, &env);

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	HSQOBJECT closure;
	sq_getstackobj(vm, 3, &closure);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_async_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	mysql_async_connection* conn = async_connections_lookup[handler];

	mysql_async_task task {
		conn,

		[](mysql_async_task& task)
		{
			task.result->value = reinterpret_cast<SQUserPointer>(!mysql_ping(task.conn->handler));
		},

		std::make_unique<mysql_async_result>(
			conn,

			[callback = Sqrat::Function(SqModule::vm, env, closure)](mysql_async_result& result) mutable
			{
				callback(result.value == reinterpret_cast<SQUserPointer>(1));

				if (Sqrat::Error::Occurred(SqModule::vm))
					Sqrat::Error::Clear(SqModule::vm);

				return false;
			}
		)
	};

	async_threadpool.get_async_thread(conn->thread_id).add_task(std::move(task));

	return 0;
}

SQInteger sq_mysql_set_character_set_async(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 4)
		return sq_throwerror(vm, "wrong number of parameters");

	HSQOBJECT env;
	sq_getstackobj(vm, 1, &env);

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	const SQChar* charset;
	sq_getstring(vm, 3, &charset);

	HSQOBJECT closure;
	sq_getstackobj(vm, 4, &closure);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_async_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	mysql_async_connection* conn = async_connections_lookup[handler];
	auto args = mysql_set_character_set_async_args(charset);

	mysql_async_task task{
		conn,

		[args](mysql_async_task& task)
		{
			SQUserPointer value = reinterpret_cast<SQUserPointer>(mysql_set_character_set(task.conn->handler, args.charset.c_str()) == 0);
			if (task.result)
				task.result->value = value;
		},

		closure._type == OT_NULL ? nullptr : std::make_unique<mysql_async_result>(
			conn,

			[callback = Sqrat::Function(SqModule::vm, env, closure)](mysql_async_result& result) mutable
			{
				callback(result.value == reinterpret_cast<SQUserPointer>(1));

				if (Sqrat::Error::Occurred(SqModule::vm))
					Sqrat::Error::Clear(SqModule::vm);

				return false;
			}
		)
	};

	async_threadpool.get_async_thread(conn->thread_id).add_task(std::move(task));

	return 0;
}

SQInteger sq_mysql_real_escape_string_async(HSQUIRRELVM vm)
{
	HSQOBJECT env;
	sq_getstackobj(vm, 1, &env);

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	const SQChar* text;
	sq_getstring(vm, 3, &text);

	HSQOBJECT closure;
	sq_getstackobj(vm, 4, &closure);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_async_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	mysql_async_connection* conn = async_connections_lookup[handler];
	auto args = mysql_real_escape_string_async_args(text, strlen(text) * 2 + 1);

	mysql_async_task task{
		conn,

		[args](mysql_async_task& task)
		{
			char* buffer = new char[args.buffer_size];
			unsigned long size = mysql_real_escape_string(task.conn->handler, buffer, args.old_text.c_str(), args.old_text.size());

			if (size == -1)
			{
				delete[] buffer;
				buffer = nullptr;
			}

			task.result->value = reinterpret_cast<SQUserPointer>(buffer);
		},

		std::make_unique<mysql_async_result>(
			conn,

			[callback = Sqrat::Function(SqModule::vm, env, closure)](mysql_async_result& result) mutable
			{
				if (result.value == nullptr)
					callback(nullptr);
				else
				{
					const SQChar* buffer = reinterpret_cast<const SQChar*>(result.value);

					callback(buffer);

					delete[] buffer;
				}

				if (Sqrat::Error::Occurred(SqModule::vm))
					Sqrat::Error::Clear(SqModule::vm);

				return false;
			}
		)
	};

	async_threadpool.get_async_thread(conn->thread_id).add_task(std::move(task));

	return 0;
}

SQInteger sq_mysql_error_async(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 2)
		return sq_throwerror(vm, "wrong number of parameters");

	if (top == 1)
	{
		async_cached_error_mutex.lock();
		sq_pushstring(vm, async_cached_error.c_str(), -1);
		async_cached_error_mutex.unlock();

		return 1;
	}

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_async_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	ASYNC_MUTEX_GUARD(sq_pushstring(vm, mysql_error(handler), -1));
	return 1;
}

SQInteger sq_mysql_errno_async(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 2)
		return sq_throwerror(vm, "wrong number of parameters");

	if (top == 1)
	{
		async_cached_error_mutex.lock();
		sq_pushinteger(vm, async_cached_errno);
		async_cached_error_mutex.unlock();

		return 1;
	}

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_async_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	ASYNC_MUTEX_GUARD(sq_pushinteger(vm, mysql_errno(handler)));
	return 1;
}

// regular functions

SQInteger sq_mysql_connect(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 7)
		return sq_throwerror(vm, "wrong number of parameters");

	const SQChar* host;
	sq_getstring(vm, 2, &host);

	const SQChar* user;
	sq_getstring(vm, 3, &user);

	const SQChar* password;
	sq_getstring(vm, 4, &password);

	const SQChar* database;
	sq_getstring(vm, 5, &database);

	SQInteger port = 3306;
	if (top >= 6 && sq_gettype(vm, 6) == OT_INTEGER)
		sq_getinteger(vm, 6, &port);

	const SQChar* socket = nullptr;
	if (top == 7 && sq_gettype(vm, 7) == OT_STRING)
		sq_getstring(vm, 7, &socket);

	MYSQL* handler = mysql_init(NULL);

	if (!handler)
	{
		sq_pushnull(vm);
		return 1;
	}

	mysql_options(handler, MYSQL_OPT_RECONNECT, &mysql_opt_reconnect);

	if (!mysql_real_connect(handler, host, user, password, database, port, socket, CLIENT_MULTI_STATEMENTS))
	{
		sync_cached_errno = mysql_errno(handler);
		sync_cached_error = mysql_error(handler);

		mysql_close(handler);
		sq_pushnull(vm);

		return 1;
	}

	sync_connections_lookup.insert(handler);
	sq_pushuserpointer(vm, handler);

	return 1;
}

SQInteger sq_mysql_close(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_sync_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	mysql_close(handler);
	sync_connections_lookup.erase(handler);

	return 0;
}

SQInteger sq_mysql_select_db(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	const SQChar* database;
	sq_getstring(vm, 3, &database);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_sync_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	sq_pushbool(vm, mysql_select_db(handler, database) == 0);
	return 1;
}

SQInteger sq_mysql_query(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	const SQChar* query;
	sq_getstring(vm, 3, &query);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_sync_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	mysql_set_server_option(handler, MYSQL_OPTION_MULTI_STATEMENTS_OFF);

	if (mysql_query(handler, query))
	{
		sq_pushnull(vm);
		return 1;
	}

	MYSQL_RES* res = mysql_store_result(handler);
	if (!res)
	{
		sq_pushnull(vm);
		return 1;
	}

	Sqrat::Object obj_result(new mysql_result(res), vm, true);
	Sqrat::PushVar(vm, obj_result);

	return 1;
}

SQInteger sq_mysql_multi_query(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	const SQChar* query;
	sq_getstring(vm, 3, &query);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_sync_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	mysql_set_server_option(handler, MYSQL_OPTION_MULTI_STATEMENTS_ON);

	int status = mysql_query(handler, query);
	if (status)
	{
		sq_pushnull(vm);
		return 1;
	}

	Sqrat::Array array(vm);

	do
	{
		MYSQL_RES* res = mysql_store_result(handler);
		if (res)
			array.Append(Sqrat::Object(new mysql_result(res), vm, true));
		else if (mysql_errno(handler) != 0)
		{
			sq_pushnull(vm);
			return 1;
		}

		if ((status = mysql_next_result(handler)) > 0)
		{
			sq_pushnull(vm);
			return 1;
		}
	} while (status == 0);

	Sqrat::PushVar(vm, array);
	return 1;
}

SQInteger sq_mysql_insert_id(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	ASYNC_MUTEX_GUARD(sq_pushinteger(vm, mysql_insert_id(handler)));
	return 1;
}

SQInteger sq_mysql_affected_rows(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	ASYNC_MUTEX_GUARD(sq_pushinteger(vm, mysql_affected_rows(handler)));
	return 1;
}

SQInteger sq_mysql_num_rows(HSQUIRRELVM vm)
{
	if (!Sqrat::ClassType<mysql_result>::IsValidInstance(vm, 2))
		return sq_throwerror(vm, "not a valid mysql result handler");

	mysql_result* result = Sqrat::ClassType<mysql_result>::GetInstance(vm, 2);
	sq_pushinteger(vm, mysql_num_rows(result->res));

	return 1;
}

SQInteger sq_mysql_num_fields(HSQUIRRELVM vm)
{
	if (!Sqrat::ClassType<mysql_result>::IsValidInstance(vm, 2))
		return sq_throwerror(vm, "not a valid mysql result handler");

	mysql_result* result = Sqrat::ClassType<mysql_result>::GetInstance(vm, 2);
	sq_pushinteger(vm, mysql_num_fields(result->res));

	return 1;
}

SQInteger sq_mysql_fetch_row(HSQUIRRELVM vm)
{
	if (!Sqrat::ClassType<mysql_result>::IsValidInstance(vm, 2))
		return sq_throwerror(vm, "not a valid mysql result handler");

	mysql_result* result = Sqrat::ClassType<mysql_result>::GetInstance(vm, 2);
	MYSQL_FIELD* fields = mysql_fetch_fields(result->res);
	MYSQL_ROW row = mysql_fetch_row(result->res);

	if (!fields || !row)
	{
		sq_pushnull(vm);
		return 1;
	}

	sq_newarray(vm, 0);

	for (unsigned int i = 0, numFields = mysql_num_fields(result->res); i < numFields; ++i)
	{
		enum_field_types fieldType = (row[i]) ? fields[i].type : MYSQL_TYPE_NULL;

		switch (fieldType)
		{
		case MYSQL_TYPE_TINY:
		case MYSQL_TYPE_SHORT:
		case MYSQL_TYPE_LONG:
		case MYSQL_TYPE_LONGLONG:
		case MYSQL_TYPE_INT24:
		case MYSQL_TYPE_YEAR:
		case MYSQL_TYPE_BIT:
			sq_pushinteger(vm, atoi(row[i]));
			sq_arrayappend(vm, -2);
			break;

		case MYSQL_TYPE_NULL:
			sq_pushnull(vm);
			sq_arrayappend(vm, -2);
			break;

		case MYSQL_TYPE_DECIMAL:
		case MYSQL_TYPE_NEWDECIMAL:
		case MYSQL_TYPE_FLOAT:
		case MYSQL_TYPE_DOUBLE:
			sq_pushfloat(vm, atof(row[i]));
			sq_arrayappend(vm, -2);
			break;

		default:
			sq_pushstring(vm, row[i], -1);
			sq_arrayappend(vm, -2);
			break;
		}
	}

	return 1;
}

SQInteger sq_mysql_fetch_assoc(HSQUIRRELVM vm)
{
	if (!Sqrat::ClassType<mysql_result>::IsValidInstance(vm, 2))
		return sq_throwerror(vm, "not a valid mysql result handler");

	mysql_result* result = Sqrat::ClassType<mysql_result>::GetInstance(vm, 2);
	MYSQL_FIELD* fields = mysql_fetch_fields(result->res);
	MYSQL_ROW row = mysql_fetch_row(result->res);

	if (!fields || !row)
	{
		sq_pushnull(vm);
		return 1;
	}

	sq_newtable(vm);

	for (unsigned int i = 0, numFields = mysql_num_fields(result->res); i < numFields; ++i)
	{
		enum_field_types fieldType = (row[i]) ? fields[i].type : MYSQL_TYPE_NULL;

		switch (fieldType)
		{
		case MYSQL_TYPE_TINY:
		case MYSQL_TYPE_SHORT:
		case MYSQL_TYPE_LONG:
		case MYSQL_TYPE_LONGLONG:
		case MYSQL_TYPE_INT24:
		case MYSQL_TYPE_YEAR:
		case MYSQL_TYPE_BIT:
			sq_pushstring(vm, fields[i].name, -1);
			sq_pushinteger(vm, atoi(row[i]));
			sq_newslot(vm, -3, SQFalse);
			break;

		case MYSQL_TYPE_NULL:
			sq_pushstring(vm, fields[i].name, -1);
			sq_pushnull(vm);
			sq_newslot(vm, -3, SQFalse);
			break;

		case MYSQL_TYPE_DECIMAL:
		case MYSQL_TYPE_NEWDECIMAL:
		case MYSQL_TYPE_FLOAT:
		case MYSQL_TYPE_DOUBLE:
			sq_pushstring(vm, fields[i].name, -1);
			sq_pushfloat(vm, atof(row[i]));
			sq_newslot(vm, -3, SQFalse);
			break;

		default:
			sq_pushstring(vm, fields[i].name, -1);
			sq_pushstring(vm, row[i], -1);
			sq_newslot(vm, -3, SQFalse);
			break;
		}
	}

	return 1;
}

SQInteger sq_mysql_ping(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_sync_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	sq_pushbool(vm, !mysql_ping(handler));
	return 1;
}

SQInteger sq_mysql_free_result(HSQUIRRELVM vm)
{
	if (!Sqrat::ClassType<mysql_result>::IsValidInstance(vm, 2))
		return sq_throwerror(vm, "not a valid mysql result handler");

	mysql_result* result = Sqrat::ClassType<mysql_result>::GetInstance(vm, 2);
	result->free();

	return 0;
}

SQInteger sq_mysql_error(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 2)
		return sq_throwerror(vm, "wrong number of parameters");

	if (top == 1)
	{
		sq_pushstring(vm, sync_cached_error.c_str(), -1);
		return 1;
	}

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_sync_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	sq_pushstring(vm, mysql_error(handler), -1);
	return 1;
}

SQInteger sq_mysql_errno(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 2)
		return sq_throwerror(vm, "wrong number of parameters");

	if (top == 1)
	{
		sq_pushinteger(vm, sync_cached_errno);

		return 1;
	}

	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_sync_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	sq_pushinteger(vm, mysql_errno(handler));
	return 1;
}

SQInteger sq_mysql_info(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	ASYNC_MUTEX_GUARD(sq_pushstring(vm, mysql_info(handler), -1));
	return 1;
}

SQInteger sq_mysql_stat(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	ASYNC_MUTEX_GUARD(sq_pushstring(vm, mysql_stat(handler), -1));
	return 1;
}

SQInteger sq_mysql_sqlstate(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	ASYNC_MUTEX_GUARD(sq_pushstring(vm, mysql_sqlstate(handler), -1));
	return 1;
}

SQInteger sq_mysql_warning_count(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	ASYNC_MUTEX_GUARD(sq_pushinteger(vm, mysql_warning_count(handler)));
	return 1;
}

SQInteger sq_mysql_get_character_set_info(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	MY_CHARSET_INFO info;
	ASYNC_MUTEX_GUARD(mysql_get_character_set_info(handler, &info));

	sq_newtable(vm);

	sq_pushstring(vm, "number", -1);
	sq_pushinteger(vm, info.number);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "state", -1);
	sq_pushinteger(vm, info.state);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "csname", -1);
	sq_pushstring(vm, info.csname, -1);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "name", -1);
	sq_pushstring(vm, info.name, -1);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "comment", -1);
	sq_pushstring(vm, info.comment, -1);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "dir", -1);
	sq_pushstring(vm, info.dir, -1);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "mbminlen", -1);
	sq_pushinteger(vm, info.mbminlen);
	sq_newslot(vm, -3, SQFalse);

	sq_pushstring(vm, "mbmaxlen", -1);
	sq_pushinteger(vm, info.mbmaxlen);
	sq_newslot(vm, -3, SQFalse);

	return 1;
}

SQInteger sq_mysql_character_set_name(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	ASYNC_MUTEX_GUARD(sq_pushstring(vm, mysql_character_set_name(handler), -1));
	return 1;
}

SQInteger sq_mysql_set_character_set(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	const SQChar* charset;
	sq_getstring(vm, 3, &charset);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_sync_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	sq_pushbool(vm, mysql_set_character_set(handler, charset) == 0);
	return 1;
}

SQInteger sq_mysql_escape_string(HSQUIRRELVM vm)
{
	const SQChar* str;
	sq_getstring(vm, 2, &str);

	size_t str_length = strlen(str);
	if (str_length == 0)
	{
		sq_pushnull(vm);
		return 1;
	}

	std::unique_ptr<char[]> buffer(new char[str_length * 2 + 1]);
	unsigned long size = mysql_escape_string(buffer.get(), str, str_length);

	if (size != -1)
		sq_pushstring(vm, buffer.get(), strlen(buffer.get()));
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_mysql_real_escape_string(HSQUIRRELVM vm)
{
	SQUserPointer ptr;
	sq_getuserpointer(vm, 2, &ptr);

	const SQChar* str;
	sq_getstring(vm, 3, &str);

	MYSQL* handler = reinterpret_cast<MYSQL*>(ptr);
	if (!is_valid_mysql_sync_connection(handler))
		return sq_throwerror(vm, "not a valid mysql connection handler");

	size_t str_length = strlen(str);
	if (str_length == 0)
	{
		sq_pushnull(vm);
		return 1;
	}

	std::unique_ptr<char[]> buffer(new char[str_length * 2 + 1]);
	unsigned long size = mysql_real_escape_string(handler, buffer.get(), str, str_length);

	if (size != -1)
		sq_pushstring(vm, buffer.get(), strlen(buffer.get()));
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_mysql_option_reconnect(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 2)
		return sq_throwerror(vm, "wrong number of parameters");

	if (top == 1)
	{
		sq_pushbool(vm, mysql_opt_reconnect);
		return 1;
	}

	SQBool arg;
	sq_getbool(vm, 2, &arg);

	mysql_opt_reconnect = (arg == 1);
	return 0;
}

SQInteger sq_mysql_option_thread_count(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 2)
		return sq_throwerror(vm, "wrong number of parameters");

	if (top == 1)
	{
		sq_pushbool(vm, async_threadpool.get_thread_count());
		return 1;
	}

	SQInteger arg;
	sq_getinteger(vm, 2, &arg);

	async_threadpool.resize(arg);
	return 0;
}

SQInteger sq_mysql_option_result_timer_interval(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 2)
		return sq_throwerror(vm, "wrong number of parameters");

	if (top == 1)
	{
		sq_pushinteger(vm, getTimerInterval(mysql_async_timer_id));
		return 1;
	}

	SQInteger arg;
	sq_getinteger(vm, 2, &arg);

	setTimerInterval(mysql_async_timer_id, arg);
	return 0;
}