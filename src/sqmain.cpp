#include <mysql.h>

#include "g2o_api.h"
#include "sqfunc.h"
#include "mysql_result.h"

int mysql_async_timer_id = -1;

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);

	if (mysql_library_init(0, NULL, NULL))
	{
		SqModule::Error("(MySQL) Cannot initialize mysql library!\n");
		return SQ_ERROR;
	}

	mysql_async_timer_id = setTimer(sq_onTimer, 100, 0);
	addEventHandler("onExit", sq_onExit);

	Sqrat::RootTable roottable(vm);

	/* squirreldoc (class)
	*
	* This class represents mysql result returned by the query.
	*
	* @version	0.6
	* @side		shared
	* @name		mysql_result
	*
	*/
	Sqrat::Class<mysql_result, Sqrat::NoConstructor<mysql_result>> sq_mysql_result(vm, "mysql_result");
	roottable.Bind("mysql_result", sq_mysql_result);

	/* squirreldoc (func)
	*
	* This function is used to open async connection to a MySQL server.  
	* Async connections allows you to communicate with database without blocking the main thread that scripts are running on.
	*
	* @version	0.6
	* @side		shared
	* @name		mysql_connect_async
	* @param	(string) host the MySQL server host. It can also include a port number. e.g. "hostname:port" or a path to a local socket e.g. ":/path/to/socket" for the localhost.
	* @param	(string) username the username login.
	* @param	(string) password the password.
	* @param	(string) database the database name that will be used by the connection.
	* @param	(int|null) port=3306 the MySQL server port, pass null to use the default value.
	* @param	(string|null) socket=null the MySQL socket file, e.g: `"/tmp/mysql.sock"`, pass null to use the default value.
	* @param	(function) callback the callback that will receive connection as argument, or null when query doesn't return result or when error occurs.
	* @return	(null|userpointer) returns a MySQL connection handler on success or null when query doesn't return result or when error occurs.
	*
	*/
	roottable.SquirrelFunc("mysql_connect_async", sq_mysql_connect_async, 8, ".ssss..c");
	/* squirreldoc (func)
	*
	* This function is used to close MySQL async connection.
	*
	* @version	0.6
	* @side		shared
	* @name		mysql_close_async
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect_async](../mysql_connect_async).
	* @param	(function) callback=null the optional callback that will be called when connection gets closed.
	*
	*/
	roottable.SquirrelFunc("mysql_close_async", sq_mysql_close_async, -2, ".pc");
	/* squirreldoc (func)
	*
	* This function is used to select a MySQL database for opened async connection.
	*
	* @version	0.6
	* @side		shared
	* @name		mysql_select_db_async
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @param	(string) database the database name that will be used by the connection.
	* @param	(function) callback=null the option callback that will receive as argument `true` if the database was successfully selected for the connection, otherwise `false`.
	*
	*/
	roottable.SquirrelFunc("mysql_select_db_async", sq_mysql_select_db_async, -3, ".psc");
	/* squirreldoc (func)
	*
	* This function is used to execute single SQL query asynchronously on MySQL server.  
	* The difference between this function and [mysql_query](../mysql_query) is that this one won't block mainthread while executing query.
	*
	* @version	0.6
	* @side		shared
	* @name		mysql_query_async
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect_async](../mysql_connect_async).
	* @param	(string) query the SQL query that will be executed on MySQL server.
	* @param	(function) callback=null the optional callback that will receive query result as argument, or null when query doesn't return result or when error occurs.
	*
	*/
	roottable.SquirrelFunc("mysql_query_async", sq_mysql_query_async, -3, ".psc");
	/* squirreldoc (func)
	*
	* This function is used to execute multiple SQL query separated by the `;` on MySQL server.  
	* The difference between this function and [mysql_multi_query](../mysql_multi_query) is that this one won't block mainthread while executing query.
	*
	* @version	0.7.3
	* @side		shared
	* @name		mysql_multi_query_async
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect_async](../mysql_connect_async).
	* @param	(string) query the SQL query that will be executed on MySQL server.
	* @param	(function) callback=null the optional callback that will receive query results as argument, or null when error occurs.
	*
	*/
	roottable.SquirrelFunc("mysql_multi_query_async", sq_mysql_multi_query_async, -3, ".psc");
	/* squirreldoc (func)
	*
	* This function pings a server connection asynchronously or reconnect if there is no connection.
	*
	* @version	0.6
	* @side		shared
	* @note		The reconnect feature is disabled by default, and can be enabled via [mysql_option_reconnect](../mysql_option_reconnect).
	* @name		mysql_ping_async
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect_async](../mysql_connect_async).
	* @param	(function) callback the callback that will receive as argument `true` if the connection to the server MySQL server is working, otherwise `false`.
	*
	*/
	roottable.SquirrelFunc("mysql_ping_async", sq_mysql_ping_async, 3, ".pc");
	/* squirreldoc (func)
	*
	* This function sets the character set for async connection.
	*
	* @version	0.6
	* @side		shared
	* @name		mysql_set_character_set_async
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect_async](../mysql_connect_async).
	* @param	(string) charset a valid character set name.
	* @param	(function) callback=null the optional callback that will receive as argument `true` if the charset was successfully set, otherwise `false`.
	*
	*/
	roottable.SquirrelFunc("mysql_set_character_set_async", sq_mysql_set_character_set_async, -3, ".psc");
	/* squirreldoc (func)
	*
	* This function escapes special characters in a string for use in [mysql_query](../mysql_query).
	* It is identical to [mysql_escape_string](../mysql_escape_string) except that it takes a async connection handler and escapes the string according to the current character set.
	*
	* @version	0.6
	* @side		shared
	* @name		mysql_real_escape_string_async
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect_async](../mysql_connect_async).
	* @param	(string) unescaped_string the string that is to be escaped.
	* @param	(function) callback the callback that will receive esaped string as argument, or `null` on failure.e.
	*
	*/
	roottable.SquirrelFunc("mysql_real_escape_string_async", sq_mysql_real_escape_string_async, 4, ".psc");
	/* squirreldoc (func)
	*
	* This function gets the text of the error message from previous MySQL operation.
	*
	* @version	0.6.2
	* @side		shared
	* @name		mysql_error_async
	* @param	(userpointer=null) connection the connection handler which comes from a call to [mysql_connect_async](../mysql_connect_async), or no parameter if you want to receive an error that occured during connecting to database via async api.
	* @return	(string) the error text from the last MySQL function, or throws error when invalid argument is passed.
	*
	*/
	roottable.SquirrelFunc("mysql_error_async", sq_mysql_error_async, -1, ".p");
	/* squirreldoc (func)
	*
	* This function gets the error code from previous MySQL operation.
	*
	* @version	0.6.2
	* @side		shared
	* @name		mysql_errno_async
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect_async](../mysql_connect_async).
	* @return	(int) the error code from the last MySQL function, 0 if there's no error, or throws error when invalid argument is passed.
	*
	*/
	roottable.SquirrelFunc("mysql_errno_async", sq_mysql_errno_async, -1, ".p");
	/* squirreldoc (func)
	*
	* This function is used to open a connection to a MySQL server.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_connect
	* @param	(string) host the MySQL server host. It can also include a port number. e.g. "hostname:port" or a path to a local socket e.g. ":/path/to/socket" for the localhost.
	* @param	(string) username the username login.
	* @param	(string) password the password.
	* @param	(string) database the database name that will be used by the connection.
	* @param	(int|null) port=3306 the MySQL server port, pass null to use the default value.
	* @param	(string|null) socket=null the MySQL socket file, e.g: `"/tmp/mysql.sock"`, pass null to use the default value.
	* @return	(null|userpointer) returns a MySQL connection handler on success or null on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_connect", sq_mysql_connect, -5, ".ssss..");
	/* squirreldoc (func)
	*
	* This function is used to close MySQL connection.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_close
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	*
	*/
	roottable.SquirrelFunc("mysql_close", sq_mysql_close, 2, ".p");
	/* squirreldoc (func)
	*
	* This function is used to select a MySQL database for opened sync connection.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_select_db
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @param	(string) database the database name that will be used by the connection.
	* @return	(bool) `true` if the database was successfully selected for the connection, otherwise `false`.
	*
	*/
	roottable.SquirrelFunc("mysql_select_db", sq_mysql_select_db, 3, ".ps");
	/* squirreldoc (func)
	*
	* This function is used to execute single SQL query on MySQL server.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_query
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @param	(string) query the SQL query that will be executed on MySQL server.
	* @return	(null|userpointer) The result handler or `null` if query doesn't return any result.
	*
	*/
	roottable.SquirrelFunc("mysql_query", sq_mysql_query, 3, ".ps");
	/* squirreldoc (func)
	*
	* This function is used to execute multiple SQL query separated by the `;` on MySQL server.
	*
	* @version	0.7.3
	* @side		shared
	* @name		mysql_multi_query
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @param	(string) query the SQL query that will be executed on MySQL server.
	* @return	(null|[userpointer...]) The array of result handlers or `null` if query doesn't return any result.
	*
	*/
	roottable.SquirrelFunc("mysql_multi_query", sq_mysql_multi_query, 3, ".ps");
	/* squirreldoc (func)
	*
	* This function will get the ID generated in the last query.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_insert_id
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(null|int) The ID generated for an AUTO_INCREMENT column by the previous query on success, `0` if the previous query does not generate an AUTO_INCREMENT value, or `null` if no MySQL connection was established.
	*
	*/
	roottable.SquirrelFunc("mysql_insert_id", sq_mysql_insert_id, 2, ".p");
	/* squirreldoc (func)
	*
	* This function retrieves the number of affected rows from a result handler.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_affected_rows
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect) or [mysql_connect_async](../mysql_connect_async).
	* @return	(int) Returns the number of affected rows on success, and -1 if the last query failed.
	*
	*/
	roottable.SquirrelFunc("mysql_affected_rows", sq_mysql_affected_rows, 2, ".p");
	/* squirreldoc (func)
	*
	* This function retrieves the number of rows from a result handler.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_num_rows
	* @param	(mysql_result) result the query result handler which comes from a call to [mysql_query](../mysql_query) or [mysql_query_async](../mysql_query_async)].
	* @return	(null|int) The ID generated for an AUTO_INCREMENT column by the previous query on success, `0` if the previous query does not generate an AUTO_INCREMENT value, or `null` if no MySQL connection was established.
	*
	*/
	roottable.SquirrelFunc("mysql_num_rows", sq_mysql_num_rows, 2, ".x");
	/* squirreldoc (func)
	*
	* This function retrieves the number of fields from a result handler.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_num_fields
	* @param	(mysql_result) result the query result handler which comes from a call to [mysql_query](../mysql_query) or [mysql_query_async](../mysql_query_async)].
	* @return	(null|int) number of fields in the result handler, or `null` if no MySQL connection was established.
	*
	*/
	roottable.SquirrelFunc("mysql_num_fields", sq_mysql_num_fields, 2, ".x");
	/* squirreldoc (func)
	*
	* This function gets a result row as an array.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_fetch_row
	* @param	(mysql_result) result the query result handler which comes from a call to [mysql_query](../mysql_query) or [mysql_query_async](../mysql_query_async)].
	* @return	(null|array) array of values that corresponds to the fetched row, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_fetch_row", sq_mysql_fetch_row, 2, ".x");
	/* squirreldoc (func)
	*
	* This function gets a result row as a table.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_fetch_assoc
	* @param	(mysql_result) result the query result handler which comes from a call to [mysql_query](../mysql_query) or [mysql_query_async](../mysql_query_async)].
	* @return	(null|table) table of values with string indicies as row names that corresponds to the fetched row, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_fetch_assoc", sq_mysql_fetch_assoc, 2, ".x");
	/* squirreldoc (func)
	*
	* This function pings a server connection or reconnect if there is no connection.
	*
	* @version	0.2
	* @side		shared
	* @note		The reconnect feature is disabled by default, and can be enabled via [mysql_option_reconnect](../mysql_option_reconnect).
	* @name		mysql_ping
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(bool) `true` if the connection to the server MySQL server is working, otherwise `false`.
	*
	*/
	roottable.SquirrelFunc("mysql_ping", sq_mysql_ping, 2, ".p");
	/* squirreldoc (func)
	*
	* This function will free result memory.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_free_result
	* @param	(mysql_result) result the query result handler which comes from a call to [mysql_query](../mysql_query) or [mysql_query_async](../mysql_query_async)].
	*
	*/
	roottable.SquirrelFunc("mysql_free_result", sq_mysql_free_result, 2, ".x");
	/* squirreldoc (func)
	*
	* This function gets the text of the error message from previous MySQL operation.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_error
	* @param	(userpointer=null) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect), or no parameter if you want to receive an error that occured during connecting to database via sync api.
	* @return	(string) the error text from the last MySQL function, or throws error when invalid argument is passed.
	*
	*/
	roottable.SquirrelFunc("mysql_error", sq_mysql_error, -1, ".p");
	/* squirreldoc (func)
	*
	* This function gets the error code from previous MySQL operation.
	*
	* @version	0.2
	* @side		shared
	* @name		mysql_errno
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @return	(int) the error code from the last MySQL function, 0 if there's no error, or throws error when invalid argument is passed.
	*
	*/
	roottable.SquirrelFunc("mysql_errno", sq_mysql_errno, -1, ".p");
	/* squirreldoc (func)
	*
	* This function gets the information about the most recent query.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_info
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect) or [mysql_connect_async](../mysql_connect_async).
	* @return	(null|string) information about the statement on success, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_info", sq_mysql_info, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the current system status.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_stat
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect) or [mysql_connect_async](../mysql_connect_async).
	* @return	(null|string) a string with the status for uptime, threads, queries, open tables, flush tables and queries per second, or `null` on failure. For a complete list of other status variables, you have to use the `SHOW STATUS` SQL command.
	*
	*/
	roottable.SquirrelFunc("mysql_stat", sq_mysql_stat, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the text of the error message for the most recently executed SQL statement.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_sqlstate
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect) or [mysql_connect_async](../mysql_connect_async).
	* @return	(null|string) the error text from recently executed SQL statement, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_sqlstate", sq_mysql_sqlstate, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the number of errors, warnings, and notes generated during execution of the previous SQL statement.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_warning_count
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect) or [mysql_connect_async](../mysql_connect_async).
	* @return	(null|int) the warning count, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_warning_count", sq_mysql_warning_count, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the client character set info.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_get_character_set_info
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect) or [mysql_connect_async](../mysql_connect_async).
	* @return	(null|{number, state, csname, name, comment, dir, mbminlen, mbmaxlen}) the info about currently used character set by the connection, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_get_character_set_info", sq_mysql_get_character_set_info, 2, ".p");
	/* squirreldoc (func)
	*
	* This function gets the client character set.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_character_set_name
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect) or [mysql_connect_async](../mysql_connect_async).
	* @return	(null|string) the current used character set by the connection, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_character_set_name", sq_mysql_character_set_name, 2, ".p");
	/* squirreldoc (func)
	*
	* This function sets the character set for connection.
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_set_character_set
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @param	(string) charset a valid character set name.
	* @return	(bool) `true` if the charset was successfully set, otherwise `false`.
	*
	*/
	roottable.SquirrelFunc("mysql_set_character_set", sq_mysql_set_character_set, 3, ".ps");
	/* squirreldoc (func)
	*
	* This function escapes special characters in a string for use in [mysql_query](../mysql_query).
	*
	* @version	0.4
	* @side		shared
	* @name		mysql_escape_string
	* @param	(string) unescaped_string the string that is to be escaped.
	* @return	(null|string) the escaped string, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_escape_string", sq_mysql_escape_string, 2, ".s");
	/* squirreldoc (func)
	*
	* This function escapes special characters in a string for use in [mysql_query](../mysql_query).  
	* It is identical to [mysql_escape_string](../mysql_escape_string) except that it takes a connection handler and escapes the string according to the current character set.
	*
	* @version	0.3
	* @side		shared
	* @name		mysql_real_escape_string
	* @param	(userpointer) connection the connection handler which comes from a call to [mysql_connect](../mysql_connect).
	* @param	(string) unescaped_string the string that is to be escaped.
	* @return	(null|string) the escaped string, or `null` on failure.
	*
	*/
	roottable.SquirrelFunc("mysql_real_escape_string", sq_mysql_real_escape_string, 3, ".ps");
	/* squirreldoc (func)
	*
	* This function gets or sets the mysql reconnect option.
	*
	* @version	0.4.2
	* @side		shared
	* @note		You should call a setter before creating new connections via [mysql_connect](../mysql_connect) or [mysql_connect_async](../mysql_connect_async).
	* @name		mysql_option_reconnect
	* @param	(bool) reconnect=optional This parameter is optional, you have to specify it to call a setter. Pass `true` if you want to enable built in mysql reconnect, otherwise `false`.
	* @return	(void|bool) nothing or `bool` if you've called getter.
	*
	*/
	roottable.SquirrelFunc("mysql_option_reconnect", sq_mysql_option_reconnect, -1, ".b");

	/* squirreldoc (func)
	*
	* This function gets or sets the mysql thread count used by async interface.
	*
	* @version	0.6
	* @side		shared
	* @note		You should call a setter before creating new connections via [mysql_connect_async](../mysql_connect_async).
	* @name		mysql_option_thread_count
	* @param	(int) count=optional This parameter is optional, you have to specify it to call a setter. Pass `integer` if you want to set a number of threads used by the async functions, default value is `1`.
	* @return	(void|integer) nothing or `integer` if you've called getter.
	*
	*/
	roottable.SquirrelFunc("mysql_option_thread_count", sq_mysql_option_thread_count, -1, ".i");

	/* squirreldoc (func)
	*
	* This function gets or sets the mysql async result timer delay, such timer is being used internally by the module to process results from other threads.
	*
	* @version	0.6.2
	* @side		shared
	* @note		You should call a setter before creating new connections via [mysql_connect_async](../mysql_connect_async).
	* @name		mysql_option_result_timer_interval
	* @param	(int) count=optional This parameter is optional, you have to specify it to call a setter. Pass `integer` if you want to set a a result timer delay, default value is `100` ms.
	* @return	(void|integer) nothing or `integer` if you've called getter.
	*
	*/
	roottable.SquirrelFunc("mysql_option_result_timer_interval", sq_mysql_option_result_timer_interval, -1, ".i");

	return SQ_OK;
}
