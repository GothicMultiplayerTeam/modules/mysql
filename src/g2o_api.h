#pragma once

#include <sqapi.h>

Sqrat::Function addEventHandler(const char* eventName, SQFUNCTION closure, SQInteger priority = 9999);

SQInteger setTimer(SQFUNCTION closure, SQInteger interval, SQInteger count);
SQInteger getTimerInterval(SQInteger timer_id);
void setTimerInterval(SQInteger timer_id, SQInteger interval);